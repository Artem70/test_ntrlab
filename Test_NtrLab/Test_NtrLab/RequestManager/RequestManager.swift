//
//  RequestManager.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class RequestManager {
    
    var alamofireManager: SessionManager?
    
    // Singleton
    static let instance = RequestManager()
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        alamofireManager = SessionManager(configuration: configuration)
    }
    
    let Url = "https://dl.dropboxusercontent.com/s/f3x990vmoxp5gis/test.json"
    
    func loadData(success: @escaping ((ModelsObject) -> Void),
                   failed: @escaping ((String) -> Void)) {
        Alamofire.request(Url, method: .get).validate().responseJSON { response in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                
                let object = ModelsObject.init(json: json)
                
                
                success(object)
                
            case .failure(let error):
                failed(error.localizedDescription)
            }
        }
    }
}
