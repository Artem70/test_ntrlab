//
//  MainViewController.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import Result


class MainViewController: UIViewController {
    
    var (lifetime, token) = Lifetime.make()
    
    @IBOutlet weak var mainView: MainView!
    
    let viewModel: MainViewModel
    
    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bind()
        viewModel.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLayoutSubviews() {
        mainView.gradientLayer.frame = CGRect(x: 0, y: 0 , width: self.view.bounds.width, height: self.view.bounds.height)
       
        
    }
}

//MARK: Setup
extension MainViewController {
    
    func setupView() {
        
    }
}

// MARK: Binding
private extension MainViewController {
    
        var transition: BindingTarget<TransistionType> {
            return BindingTarget(lifetime: lifetime, action: { [weak self] (type) in
                guard let strongSelf = self else {
                    return
                }
                switch type {
    
                case .push(let controller):
                    strongSelf.navigationController?.pushViewController(controller, animated: true)
                case .present(let controller):
                    strongSelf.present(controller, animated: true, completion: nil)
                case .setRootWindow(_ ): break
                    //UIApplication.shared.keyWindow?
                case .pop:
                    strongSelf.navigationController?.popViewController(animated: true)
                case .dismiss:
                    strongSelf.presentingViewController?.dismiss(animated: true, completion: nil)
                }
            })
        }
    
    var showError: BindingTarget<(String)>{
        return BindingTarget(lifetime: lifetime, action: { [weak self] error in
            
            guard let strongSelf = self else {
                return
            }
            strongSelf.showAlertError(messageTitle: "Error", message: error)
        })
    }
    
    var dataModel: BindingTarget<(ModelsObject)>{
        return BindingTarget(lifetime: lifetime, action: { [weak self] modelsObject in
            
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.mainView.productsLabelOne.text = modelsObject.products[0]
            strongSelf.mainView.productLabelTwo.text = modelsObject.products[1]
            strongSelf.mainView.phoneLabel.text =  modelsObject.phone
            strongSelf.mainView.streetLabel.text = modelsObject.street
            strongSelf.mainView.countryLabel.text = modelsObject.country
            strongSelf.mainView.cityLabel.text = modelsObject.city
            strongSelf.mainView.hide(false)
            
        })
    }
    
    
    func bind() {
        mainView.spinner.reactive.isAnimating <~ viewModel.loading
        showError  <~ viewModel.showError
        dataModel <~ viewModel.dataModel
        viewModel.button <~ mainView.buttonPressed
        transition <~ viewModel.navigation
    }
}



