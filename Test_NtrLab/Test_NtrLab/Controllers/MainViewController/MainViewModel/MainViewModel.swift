//
//  MainViewModel.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result

enum TransistionType {
    case push(controller: UIViewController)
    case present(controller: UIViewController)
    case setRootWindow(controller: UIViewController)
    case dismiss
    case pop
}

class MainViewModel {

    //MARK: Reactive
    var (lifetime, token) = Lifetime.make()
    
    var object: ModelsObject?
    
    // MARK: Reactive
    var loading: Property<Bool> { return Property(_loading) }
    fileprivate var _loading: MutableProperty<Bool> = MutableProperty(false)
    
    var showError: Signal<(String), NoError>
    fileprivate var showErrorObserver: Signal<(String), NoError>.Observer
    
    var dataModel: Signal<(ModelsObject), NoError>
    fileprivate var dataModelObserver: Signal<(ModelsObject), NoError>.Observer
    
    var navigation: Signal<TransistionType, NoError>
    fileprivate var navigationObserver: Signal<TransistionType, NoError>.Observer
    
    init() {
        (navigation, navigationObserver) = Signal.pipe()
        (showError, showErrorObserver) = Signal.pipe()
        (dataModel, dataModelObserver) = Signal.pipe()
    }
}

// MARK: Networking
extension MainViewModel {
    func loadData() {
        _loading.value = true
        RequestManager.instance.loadData(success: { [weak self] modelObject in
            guard let strongSelf = self else {
                return
            }
            
            print(modelObject)
            
            strongSelf.dataModelObserver.send(value: modelObject)
            
            strongSelf._loading.value = false
        }) {  [weak self] error in
            
            guard let strongSelf = self else {
                return
            }
            strongSelf.showErrorObserver.send(value: error)
            strongSelf._loading.value = false
        }
    }
}

// MARK: Supporting methods
extension MainViewModel {
    
    var button: BindingTarget<()> {
        return BindingTarget(lifetime: lifetime, action: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            let vc = MapViewController()
            strongSelf.navigationObserver.send(value: TransistionType.push(controller: vc))
        })
    }
}
