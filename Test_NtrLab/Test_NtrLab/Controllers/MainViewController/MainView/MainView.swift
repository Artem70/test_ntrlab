//
//  MainView.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class MainView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var buttonOutlet: UIButton!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var productsLabelOne: UILabel!
    @IBOutlet weak var productLabelTwo: UILabel!
    
    
    //MARK: Reactive Cocoa Signal
    var buttonPressed: Signal<(), NoError>
    fileprivate var buttonPressedObserver: Signal<(), NoError>.Observer
    
    
    var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 1)
            let startColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1).cgColor
            let endColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1).cgColor
            gradientLayer.colors = [startColor,endColor]
        }
    }
    
    // MARK: Lifecycle
    override init(frame: CGRect) {
        (buttonPressed, buttonPressedObserver) = Signal.pipe()
        super.init(frame: frame)
        loadViewFromNib()
        setupView()
    }
    
    // MARK: init from nib
    required init?(coder aDecoder: NSCoder) {
        (buttonPressed, buttonPressedObserver) = Signal.pipe()
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
}

//MARK: Action
extension MainView {
    @IBAction func buttonPressed(_ sender: UIButton) {
        buttonOutlet.shake()
        buttonPressedObserver.send(value: ())
    }
}

//MARK: Setup
extension MainView {
    
    func setupView() {
        spinner.hidesWhenStopped = true
        hide(true)
        gradientLayer = CAGradientLayer()
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func hide(_ bool: Bool) {
        cityLabel.isHidden = bool
        streetLabel.isHidden = bool
        countryLabel.isHidden = bool
        productsLabelOne.isHidden = bool
        productLabelTwo.isHidden = bool
        phoneLabel.isHidden = bool
        
    }
    
    func loadViewFromNib() {
        view = NibLoader.load(MainView.self, addAsSubviewTo: self)
    }
}
