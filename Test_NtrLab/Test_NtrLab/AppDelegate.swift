//
//  AppDelegate.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupApp()
        load()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

//MARK: Load Window
private extension AppDelegate {
    
    func load() {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let vm = MainViewModel()
        let vc = MainViewController(viewModel: vm)
        window?.rootViewController = NavBarController.createNavBar(viewController: vc)
        window?.makeKeyAndVisible()
    }
}

//MARK: Setup
private extension AppDelegate {
    
    func setupApp() {
        //appearance - внешность
        UINavigationBar.appearance().barTintColor = AppColor.lightBlueColor()
        //tintColor - цвет тайтлов
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        //UIBarButtonItem.appearance() - title button back
        UIBarButtonItem.appearance().setTitleTextAttributes(
            [NSAttributedStringKey.foregroundColor: UIColor.clear], for: .highlighted)
        UIBarButtonItem.appearance().setTitleTextAttributes(
            [NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)
        
        //        UITabBar.appearance().barTintColor = UIColor.white
        //        UITabBar.appearance().tintColor = AppColor.greenColor()
    }
}

