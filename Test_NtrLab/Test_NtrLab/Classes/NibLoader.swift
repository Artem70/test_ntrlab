//
//  NibLoader.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import UIKit

public class NibLoader {
    public static func load<T: UIView>(
        _ type: T.Type,
        addAsSubviewTo view: UIView,
        fileOwner: AnyObject? = nil,
        useAutoLayout: Bool = true) -> UIView? {
        var owner = fileOwner
        if owner == nil {
            owner = view
        }
        
        let nib = self.nib(forType: type)
        
        guard let loadedView = self.view(fromNib: nib, nibFileOwner: owner) else {
            return nil
        }
        
        view.addSubview(loadedView)
        if useAutoLayout {
            view.attachSubviewUsingConstraints(subview: loadedView)
        } else {
            loadedView.frame = view.bounds
            loadedView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
        
        return loadedView
    }
    
    public static func nib(forType type: Swift.AnyClass) -> UINib {
        let nibName = self.nibName(forType: type)
        let bundle = Bundle(for: type)
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib
    }
}

// MARK: Supporting methods
private extension NibLoader {
    static func nibName(forType type: Swift.AnyClass) -> String {
        let fullTypeName = NSStringFromClass(type)
        
        let nameComponents = fullTypeName.components(separatedBy: ".")
        if let lastNameComponent = nameComponents.last {
            return lastNameComponent
        } else {
            return fullTypeName
        }
    }
    
    static func view(fromNib nib: UINib, nibFileOwner: AnyObject?) -> UIView? {
        let instantiatedView = nib.instantiate(withOwner: nibFileOwner, options: nil).first as? UIView
        
        return instantiatedView
    }
}

public extension UIView {
    public func attachSubviewUsingConstraints(subview: UIView) {
        assert(subview.superview == self, "superview of \(subview) has to be equal to \(self)")
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                      options: NSLayoutFormatOptions.directionLeadingToTrailing,
                                                      metrics: nil,
                                                      views: ["view": subview]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                      options: NSLayoutFormatOptions.directionLeadingToTrailing,
                                                      metrics: nil,
                                                      views: ["view": subview]))
    }
}
