//
//  AppColor.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import UIKit

class AppColor {
    
    static func greenColor() -> UIColor {
        return UIColor(displayP3Red: 72/255.0, green: 146/255.0, blue: 33/255.0, alpha: 1.0)
    }
    
    static func blueColor() -> UIColor {
        return UIColor(displayP3Red: 20/255.0, green: 86/255.0, blue: 182/255.0, alpha: 1.0)
    }
    
    static func lightBlueColor() -> UIColor {
        return UIColor(displayP3Red: 62/255.0, green: 161/255.0, blue: 228/255.0, alpha: 1.0)
    }
    
    static func darcGreyColor() -> UIColor {
        return UIColor(displayP3Red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)
    }
    
    static func silver() -> UIColor {
        return UIColor(displayP3Red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0)
    }
    
    static func text() -> UIColor {
        return UIColor(displayP3Red: 23/255.0, green: 11/255.0, blue: 1/255.0, alpha: 1.0)
    }
}
