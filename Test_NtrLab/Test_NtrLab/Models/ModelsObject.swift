//
//  ModelsObject.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

struct ModelsObject {
    
    var city: String
    var street: String
    var country: String
    var phone: String
    var products: [String]
    var gps_lat: Double
    var gps_lng: Double
    
    
    init(json: JSON) {
        let city = json["address"]["city"].stringValue
        let street = json["address"]["street"].stringValue
        let country = json["address"]["country"].stringValue
        let gps_lat = json["gps_lat"].doubleValue
        let gps_lng = json["gps_lng"].doubleValue
        let phone = json["phone"].stringValue
        
        let array = json["products"].arrayValue
        var products: [String] = []
        for value in array {
            products.append( value.stringValue)
        }
        
        self.city = city
        self.country = country
        self.street = street
        self.gps_lat = gps_lat
        self.gps_lng = gps_lng
        self.products = products
        self.phone = phone
    }
}



