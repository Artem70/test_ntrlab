//
//  UIViewController+UIAlertController.swift
//  Test_NtrLab
//
//  Created by Artem Pashkevich on 28.02.18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlertError(messageTitle: String, message: String) {
        let alertError = UIAlertController(title: messageTitle , message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok" , style: .cancel, handler: nil)
        
        alertError.addAction(okAction)
        alertError.setValue(NSAttributedString(string: messageTitle,
                                               attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20),
                                                            NSAttributedStringKey.foregroundColor : UIColor.red]),
                            forKey: "attributedTitle")
        
        self.present(alertError, animated: true, completion: nil)
    }
    
    func showSuccessAlert(message: String, messageTitle: String) {
        let alert = UIAlertController(title: messageTitle , message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok" , style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        
        alert.setValue(NSAttributedString(string: message,
                                          attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16),
                                                       NSAttributedStringKey.foregroundColor : UIColor.darkText]),
                       forKey: "attributedMessage")
        
        alert.setValue(NSAttributedString(string: messageTitle,
                                          attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20),
                                                       NSAttributedStringKey.foregroundColor : UIColor(displayP3Red: 72/255.0, green: 146/255.0, blue: 33/255.0, alpha: 1.0)]),
                       forKey: "attributedTitle")
        
        self.present(alert, animated: true, completion: nil)
    }
}
